<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="/TpMaven/index">SlipWorld</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
      <li class="nav-item" id="contact">
        <a class="nav-link" href="/TpMaven/categorie">Catégories</a>
      </li>
      
        <li class="nav-item" id="contact">
              <a class="nav-link" href="/TpMaven/contact">Contact</a>
        </li>
        
     
        
        
       <c:choose>
           <c:when test="${sessionScope.login != null}">
                   <li class="nav-item">
                         <a class="nav-link" href="/TpMaven/admin">Admin</a>
                   </li>
                   <li class="nav-item">
                         <a class="nav-link" href="/TpMaven/logoff">Se déconnecter</a>
                   </li>
            </c:when>
            <c:otherwise>
             <li class="nav-item">
                   <a class="nav-link" href="/TpMaven/login">Se connecter</a>
             </li>
            </c:otherwise>
       </c:choose>



        

    </ul>
  </div>
</nav>