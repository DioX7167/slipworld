<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  
  
  <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
  <!-- Bootstrap CSS et JS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<title>Cat�gorie</title>
</head>
<body>

<!-- Nav-->
<jsp:include page="../inc/nav.jsp"></jsp:include>

	<h1 class="text-center" id="titre-categorie">Enfants - Slips usag�s</h1>
	<div class="container" id="categorie">
                      
            <div class="row">
                <div class="col-sm-3">
                        <a href="article.jsp/1">
                                <img src="<c:url value="/resources/images/slip-enfant-1.jpg"/>" alt="" />
                        </a>
                </div>
                <div class="col-sm-3">
                        <a href="article.jsp/2">
                                <img src="<c:url value="/resources/images/slip-enfant-2.jpg"/>" alt="" />
                        </a>
                </div>
                <div class="col-sm-3">
                        <a href="article.jsp/3">
                                <img src="<c:url value="/resources/images/slip-enfant-3.jpg"/>" alt="" />
                        </a>
                </div>
                <div class="col-sm-3">
                        <a href="article.jsp/4">
                                <img src="<c:url value="/resources/images/slip-enfant-4.jpg"/>" alt="" />
                        </a>
                </div>

                <div class="col-sm-3">
                        <a href="article.jsp/5">
                                <img src="<c:url value="/resources/images/slip-enfant-5.jpg"/>" alt="" />
                        </a>
                </div>
                <div class="col-sm-3">
                        <a href="article.jsp/6">
                                <img src="<c:url value="/resources/images/slip-enfant-6.jpg"/>" alt="" />
                        </a>
                </div>
                <div class="col-sm-3">
                        <a href="article.jsp/7">
                                <img src="<c:url value="/resources/images/slip-enfant-7.jpg"/>" alt="" />
                        </a>
                </div>
                <div class="col-sm-3">
                        <a href="article.jsp/8">
                                <img src="<c:url value="/resources/images/slip-enfant-8.jpg"/>" alt="" />
                        </a>
                </div>

            </div>
        </div>
	
	<!-- Footer -->
	<jsp:include page="../inc/footer.jsp"></jsp:include>
</body>
</html>