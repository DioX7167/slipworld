<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">

  
  <!-- Bootstrap CSS et JS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<title>Slip World</title>
</head>
<body>
	
<!-- Nav-->
<jsp:include page="../inc/nav.jsp"></jsp:include>
	
<div class="container-fluid">
	<div class="row">
		<div class="col-6">
                    <div id="carouselPresentation" class="carousel slide carousel-fade" data-ride="carousel">
			  <div class="carousel-inner text-center">
				<div class="carousel-item active">
				  <p>La nouvelle gamme de culottes nautique est arriv�e</p>
				  <a href="">
					<img src="<c:url value="/resources/images/slip-enfant.jpg"/>" class="d-block" alt="Slip pour Enfant">
				  </a>
				</div>
				<div class="carousel-item text-center">
				  <p>Choisis tes h�ros pr�f�r�s, et fait les imprimer sur tes sous v�tements !!</p>
				  <a href="">
					<img src="<c:url value="/resources/images/slip-hero.jpg"/>" class="d-block" alt="Slip H�ros">
				  </a>
				</div>
				<div class="carousel-item text-center">
				  <p>Les culottes fantaisies font toujours autant de ravage chez les moins de 18 ans !! R�servez la votre maintenant !</p>
				  <a href="">
					<img src="<c:url value="/resources/images/slip-fantaisie.jpg"/>" class="d-block" alt="Slip Fantaisie">
				  </a>
				</div>
			  </div>
			  <a class="carousel-control-prev" href="#carouselPresentation" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Pr�c�dent</span>
			  </a>
			  <a class="carousel-control-next" href="#carouselPresentation" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Suivant</span>
			  </a>
                    </div>
		</div>
		<div class="col-6">
                    <h4>
                      Envie de discuter avec des particuliers ?
                      <small class="text-muted">La passion avant tout</small>
                    </h4>
                    <h1>${sessionScope.login}</h1>
                    <hr>
                    <form>
                        <input id="message" type="text" class="form-control" id="exampleInputPassword1" placeholder="Entrer votre message..">
                        <br>
                        <button type="button" class="btn btn-primary" onclick="doSend();">Envoyer</button>
                        
                        

                    </form>
                    <br>

                    <textarea readonly class="form-control" id="echoText" rows="14"></textarea>
		</div>

	</div>
                                  
        </br>
</div>
    
    <script type="text/javascript">
        <c:set var="pseudo" value="${sessionScope.login}"/> 

        
        var vpseudo="${pseudo}";
        
        if (vpseudo == ""){
            vpseudo = "Guest"
        }
        
        
        
        var echoText = document.getElementById("echoText");
        echoText.value = "";
        var Txtmessage = document.getElementById("message");

       
        
        function testWebSocket() {
         websocket = new WebSocket("ws://" + window.location.host + "/TpMaven/websocketendpoint");
			
         websocket.onopen = function(evt) {
            onOpen(evt)
         };
		
         websocket.onmessage = function(evt) {
            onMessage(evt)
         };
		
         websocket.onerror = function(evt) {
            onError(evt)
         };
      }
      var websocket = null;
      testWebSocket();
		
      function onOpen(evt) {
         writeToScreen("CONNECTED");
         Txtmessage.value= vpseudo + " est connect�";
         doSend();
      }
		
      function onMessage(evt) {
         writeToScreen(evt.data); //websocket.close();
      }

      function onError(evt) {
         writeToScreen(evt.data);
      }
		
      function doSend() {
         writeToScreen("Moi : " + Txtmessage.value); websocket.send(vpseudo +" : "+Txtmessage.value); Txtmessage.value = "";
      }
		
      function writeToScreen(message) {
          
         echoText.value += message + "\n";
      }
      
      $(document).on('keypress',function(e) {
          
            if(e.which == 13) {
                e.preventDefault();
                doSend();
            }
        });
    </script>
<!-- Footer -->
<jsp:include page="../inc/footer.jsp"></jsp:include>
	
</body>
</html>