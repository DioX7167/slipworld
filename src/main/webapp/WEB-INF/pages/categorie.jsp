<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  
  
  <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
  <!-- Bootstrap CSS et JS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<title>Cat�gorie</title>
</head>
<body>

<!-- Nav-->
<jsp:include page="../inc/nav.jsp"></jsp:include>

	<h1 class="text-center" id="titre-categorie">Nos cat�gories de Slips</h1>
	<div class="container" id="categorie">
            
            <div class="row">              
                <div class="col-sm-12">
                    
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                          <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Nos slips usag�s
                              </button>
                            </h2>
                          </div>

                          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                              Quoi de mieux que d'avoir un slip d�dicac�. Venez parcourir notre biblioth�ques de slips sales !
                              (Lien actif sur la cat�gorie "Enfant" seulement)
                              <hr>

                              <div class="row">
                                  <div class="col-sm-3">
                                      <h2>Homme</h2>
                                      <img src="<c:url value="/resources/images/homme.jpg"/>" alt="" />
                                  </div>
                                  <div class="col-sm-3">
                                      <h2>Femme</h2>
                                      <img src="<c:url value="/resources/images/femme.jpg"/>" alt="" />
                                  </div>
                                  <div class="col-sm-3">
                                      <h2>Enfant</h2>
                                      <a href="categorie/enfant-use">
                                        <img src="<c:url value="/resources/images/enfant.jpg"/>" alt="" />
                                      </a>                                  
                                  </div>
                                  <div class="col-sm-3">
                                      <h2>Non Binaire</h2>
                                      <img src="<c:url value="/resources/images/non-binaire.jpg"/>" alt="" />
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Nos slips neufs
                              </button>
                            </h2>
                          </div>
                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                              
                              Venez d�couvrir toutes les nouvelles gammes de slips neufs pour tout le monde !
                              
                              <div class="row">
                                  <div class="col-sm-3">
                                      <h2>Homme</h2>
                                      <img src="<c:url value="/resources/images/homme.jpg"/>" alt="" />
                                  </div>
                                  <div class="col-sm-3">
                                      <h2>Femme</h2>
                                      <img src="<c:url value="/resources/images/femme.jpg"/>" alt="" />
                                  </div>
                                  <div class="col-sm-3">
                                      <h2>Enfant</h2>
                                      <img src="<c:url value="/resources/images/enfant.jpg"/>" alt="" />
                                  </div>
                                  <div class="col-sm-3">
                                      <h2>Non Binaire</h2>
                                      <img src="<c:url value="/resources/images/non-binaire.jpg"/>" alt="" />
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                    </div>              
                </div>
            
           
            </div>
        </div>
                                  
        </br>
	
	<!-- Footer -->
	<jsp:include page="../inc/footer.jsp"></jsp:include>
</body>
</html>