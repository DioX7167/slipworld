package org.cesi.formation.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cesi.formation.models.Bdd;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			Bdd bd = new Bdd();
			Statement statement = bd.connexion.createStatement();
			ResultSet resultat = statement.executeQuery( "SELECT pseudo_utilisateur, mdp_utilisateur, droit_utilisateur FROM utilisateurs;" );
			
			while ( resultat.next() ) {

			    String motDePasseUtilisateur = resultat.getString( "mdp_utilisateur" );

			    String pseudoUtilisateur = resultat.getString( "pseudo_utilisateur" );
			    
			    if(request.getParameter("login").equals(pseudoUtilisateur) && request.getParameter("mdp").equals(motDePasseUtilisateur)) {
					request.getSession().setAttribute("login",request.getParameter("login"));
					request.getSession().setAttribute("auth",resultat.getString( "droit_utilisateur"));
				}

			}
			request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}

}
