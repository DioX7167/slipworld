package org.cesi.formation.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cesi.formation.models.Bdd;
import org.cesi.formation.models.Utilisateur;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/admin")
public class AdminIndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminIndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Bdd bd = new Bdd();
		Statement statement;
		try {
			statement = bd.connexion.createStatement();
		
			ResultSet resultat = statement.executeQuery( "SELECT * FROM utilisateurs;" );
			List<Utilisateur> listU = new ArrayList<Utilisateur>();
			while ( resultat.next() ) {
	
				listU.add(new Utilisateur(resultat.getString("id_utilisateur"),resultat.getString( "nom_utilisateur"),resultat.getString( "prenom_utilisateur"),resultat.getString( "pseudo_utilisateur"),resultat.getString( "droit_utilisateur")));
			}
			request.setAttribute("listU", listU);
			request.getRequestDispatcher("/WEB-INF/admin/index.jsp").forward(request, response);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
