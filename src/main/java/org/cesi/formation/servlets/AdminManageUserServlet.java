package org.cesi.formation.servlets;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cesi.formation.models.Bdd;
import org.cesi.formation.models.Utilisateur;

/**
 * Servlet implementation class AdminManageUserServlet
 */
@WebServlet("/admin/managerUser")
public class AdminManageUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminManageUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Bdd bd = new Bdd();
		PreparedStatement statement;
		try {
			statement = bd.connexion.prepareStatement("SELECT * FROM utilisateurs WHERE id_utilisateur=?");
			statement.setString(1,request.getParameter("id")); 
			ResultSet resultat = statement.executeQuery();
			
			while ( resultat.next() ) {
	
				request.setAttribute("user",new Utilisateur(resultat.getString("id_utilisateur"),resultat.getString( "nom_utilisateur"),resultat.getString( "prenom_utilisateur"),resultat.getString( "pseudo_utilisateur"),resultat.getString( "droit_utilisateur")));
			}
			request.getRequestDispatcher("/WEB-INF/admin/managerUser.jsp").forward(request, response);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
