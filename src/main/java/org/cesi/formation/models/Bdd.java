package org.cesi.formation.models;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Driver;
public class Bdd {
	public Connection connexion;
	
	public Bdd() {

		/* Connexion � la base de donn�es */
		String url = "jdbc:mysql://localhost:3306/pjee?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String utilisateur = "root";
		String motDePasse = "";
		
		try {
			Class.forName( "com.mysql.cj.jdbc.Driver" );
		    this.connexion = DriverManager.getConnection( url, utilisateur, motDePasse );

		} catch ( SQLException e ) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void Close() {
		try {
            
            connexion.close();
        } catch ( SQLException ignore ) {
            
        }
	}
}
