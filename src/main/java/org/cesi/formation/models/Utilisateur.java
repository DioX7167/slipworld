package org.cesi.formation.models;

public class Utilisateur {
	public String id;
	public String nom;
	public String prenom;
	public String pseudo;
	public String droit;
	
	public Utilisateur(String id, String nom, String prenom,String pseudo,String droit) {
		this.id=id;
		this.nom=nom;
		this.prenom=prenom;
		this.pseudo=pseudo;
		this.droit=droit;
		
	}

	public String getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getPseudo() {
		return pseudo;
	}
	
	public String getDroit() {
		return droit;
	}

}
