<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Administration</title>
</head>
<body>
	
	<c:forEach items="${listU}" var="user">

		<a href="${pageContext.servletContext.contextPath}/admin/managerUser?id=<c:out value="${user.id}" />"><c:out value="${user.pseudo}" /> </a>
	</c:forEach>

</body>
</html>