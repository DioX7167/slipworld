<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

  <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
  
  <!-- Bootstrap CSS et JS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<title>Contact</title>
</head>
<body>

	<!-- Nav-->
	<jsp:include page="../inc/nav.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row">
			<div class="col text-center">
				<h1>Qui�nes somos</h1>
				<p id="employesSW">				
					Somos una empresa de fabricaci�n de resbalones. La sede de la empresa est� ubicada en China y estamos bajo explotaci�n. Nuestro CEO es un tirano y nos paga tirachinas. A veces nos azota si no puede crear suficiente resbal�n a tiempo. �Ay�danos, llama a la polic�a antes de que vea esta p�gina!
				</p>
				<div id="employesSW">
					<img src="/TpMaven/resources/images/slip-societe.jpg" alt="Employ�s Slip World" />
					<p>
						De izquierda a derecha : <span class="employe">Alberto Alcaraz - Amaury Pinilla - Diego Arteta - Charly Garcia - Ghislain N��ez - Kevina Gonz�lez</span>
					</p>
				</div>
				
				<p>
					Email : slipworld@contactar.es<br/>
					T�l�phone : +34 456.45.34.20
				</p>
			</div>
		</div>
	</div>
	
	<!-- Footer -->
	<jsp:include page="../inc/footer.jsp"></jsp:include>
</body>
</html>